const mainContainer=document.getElementById('main');

mainContainer.addEventListener('click', (event)=>   // main event listner function
{
    // event.preventDefault();

    if(event.target.id==='nextStep') checkCurrentPage(); // if clicked nextStep button 

    if(event.target.id==='goBack') previousPageFunction(); // to go back 

    if(event.target.id==='toggle') switchPrices(); // if toggle button is clicked

    // select plan on click
    const selectDiv = event.target;
    const parentSelectDiv = event.target.parentElement;
    const grandparentSelectDiv = event.target.parentElement.parentElement;

    if (selectDiv.classList[0] === 'pCard') {
        planSelector(selectDiv);
    }
    else if (parentSelectDiv.classList[0] === 'pCard') {
        planSelector(parentSelectDiv);
    }
    else if (grandparentSelectDiv.classList[0] === 'pCard') {
        planSelector(grandparentSelectDiv);
    }

    //AddOn page

    if(selectDiv.classList[0]==='checkboxDiv'){
        addOnSelector(selectDiv,false);
    }
    else if(event.target.classList[0]==='checkClicked'){
        addOnSelector(parentSelectDiv,true)
    }
    else if(parentSelectDiv.classList[0] === 'checkboxDiv'){
        addOnSelector(parentSelectDiv,false);
    }
    else if (grandparentSelectDiv.classList[0] === 'checkboxDiv') {
        addOnSelector(grandparentSelectDiv,false);
    }

    if(event.target.id==='changePlans') backToPlanPage();

});

function checkCurrentPage()
{
    const infoPage=document.getElementById('personalInfo');
    const planPage=document.getElementById('plan');
    const addOnPage=document.getElementById('pick-addOn');
    const summaryPage=document.getElementById('totalPricingSummary');

    // console.log(infoPage.classList.length);

    if(infoPage.classList.length===0)
    {
        validateForm();
    }

    else if(planPage.classList.length===0)
    {
        planSelected();
    }
    else if(addOnPage.classList.length===0)
    {
        summaryPageContent();
    }
    else if(summaryPage.classList.length===0)
    {
        nextPageFunction();
    }


}

// form validation

const nextStepButton=document.getElementById('nextStep');

function validateForm(){
    

    const nameField = document.getElementById('fullName');
    const name=nameField.value;
    const emailField = document.getElementById('email');
    const email=emailField.value;
    const mobileField = document.getElementById('mobile');
    const mobile=mobileField.value;

    //error message

    const errorName = document.getElementById('name-error');
    const errorEmail = document.getElementById('email-error');
    const errorMobile = document.getElementById('mobile-error');

    let errorCount=0;

    const nonAlphabeticRegex = /[^a-zA-Z ]/;
    const numberRegex=/[^0-9]/;
    
    if(name.length < 1 ||  nonAlphabeticRegex.test(name.trim())) {

        errorName.textContent = 'Enter valid name';
        nameField.style.borderColor="red";
        ++errorCount;
    }
    else{
        nameField.style.borderColor="grey";
        errorName.textContent = '';
    }

    if(!email.includes('@')) {
        errorEmail.textContent = 'Please enter valid email ID.';
        emailField.style.borderColor="red";
        ++errorCount;
    }
    else{
        emailField.style.borderColor="grey";
        errorEmail.textContent = '';
    }


    if(mobile.trim().length < 10 || numberRegex.test(mobile.trim())) {
        errorMobile.textContent = 'Enter valid phone number';
        mobileField.style.borderColor="red";
        ++errorCount;
    }
    else{
        mobileField.style.borderColor="grey";
        errorMobile.textContent = '';
    }

    if(errorCount==0)
    {
        nextPageFunction();
    }

}


// next page function

function nextPageFunction()
{
    const infoPage=document.getElementById('personalInfo');
    const planPage=document.getElementById('plan');
    const addOnPage=document.getElementById('pick-addOn');
    const summaryPage=document.getElementById('totalPricingSummary');
    const thankYouPage=document.getElementById('thankYou');
    //side-bar numbers
    const one=document.getElementById('1');
    const two=document.getElementById('2');
    const three=document.getElementById('3');
    const four=document.getElementById('4');


    const goBack=document.getElementById('goBack');

    
    if(infoPage.classList.length===0)
    {
        infoPage.classList.add("displayOff");
        planPage.classList.remove("displayOff");
        goBack.classList.remove("displayOff");
        goBack.parentElement.style.justifyContent="space-between";
        one.classList.remove("fillColor");
        two.classList.add('fillColor');
    }

    else if(planPage.classList.length===0)
    {
        planPage.classList.add('displayOff');
        addOnPage.classList.remove('displayOff');
        two.classList.remove("fillColor");
        three.classList.add('fillColor');
    }

    else if(addOnPage.classList.length===0)
    {
        addOnPage.classList.add('displayOff');
        summaryPage.classList.remove('displayOff');
        three.classList.remove("fillColor");
        four.classList.add('fillColor');
    }
    else{
        summaryPage.classList.add('displayOff');
        thankYouPage.classList.remove('displayOff');

        document.getElementsByClassName('buttonDiv')[0].classList.add('displayOff');
    }
    

}

// previousPage Function

function previousPageFunction()
{
    const infoPage=document.getElementById('personalInfo');
    const planPage=document.getElementById('plan');
    const addOnPage=document.getElementById('pick-addOn');
    const summaryPage=document.getElementById('totalPricingSummary');

    //side-bar numbers
    const one=document.getElementById('1');
    const two=document.getElementById('2');
    const three=document.getElementById('3');
    const four=document.getElementById('4');


    const goBack=document.getElementById('goBack');

    
    if(planPage.classList.length===0)
    {
        planPage.classList.add('displayOff');
        infoPage.classList.remove('displayOff');
        two.classList.remove("fillColor");
        one.classList.add('fillColor');
        goBack.classList.add('displayOff');
        goBack.parentElement.style.justifyContent="flex-end";
    }

    else if(addOnPage.classList.length===0)
    {
        addOnPage.classList.add('displayOff');
        planPage.classList.remove('displayOff');
        three.classList.remove("fillColor");
        two.classList.add('fillColor');
    }

    else if(summaryPage.classList.length===0)
    {
        summaryPage.classList.add('displayOff');
        addOnPage.classList.remove('displayOff');
        four.classList.remove("fillColor");
        three.classList.add('fillColor');
    }
    
    

}



// switch prices from monthly to yearly

function switchPrices()
{
    const toggleButton=document.getElementById('toggle');

    const monthly=document.getElementById('monthly');
    const yearly=document.getElementById('yearly');

    const arcadePrice=document.getElementById('arcade-price');
    const advancePrice=document.getElementById('advanced-price');
    const proPrice=document.getElementById('pro-price');

    const onlineServicePrice=document.getElementById('onlineServicePrice');
    const largerStoragePrice=document.getElementById('largerStoragePrice');
    const customProfilePrice=document.getElementById('customProfilePrice');

    const freeMonths=document.querySelectorAll('.freeMonths');
    const planType=document.getElementById('plan-type');
    const totalPriceTag=document.getElementById('totalPriceCostTag');

    // console.log(freeMonths);

    if(toggleButton.checked)
    {
        monthly.style.color="hsl(231, 11%, 63%)";
        yearly.style.color="hsl(213, 96%, 18%)";

        arcadePrice.textContent='$90/yr';
        advancePrice.textContent='$120/yr';
        proPrice.textContent='$150/yr';

        onlineServicePrice.textContent='$10/yr';
        largerStoragePrice.textContent='$20/yr';
        customProfilePrice.textContent='$20/yr';

        planType.textContent='(Yearly)';
        totalPriceTag.textContent='Total (per year)';

        freeMonths.forEach((element)=>element.classList.remove('displayOff'));
    }
    else{
        monthly.style.color="hsl(213, 96%, 18%)";
        yearly.style.color="hsl(231, 11%, 63%)";

        arcadePrice.textContent='$9/mo';
        advancePrice.textContent='$12/mo';
        proPrice.textContent='$15/mo';

        onlineServicePrice.textContent='$1/mo';
        largerStoragePrice.textContent='$2/mo';
        customProfilePrice.textContent='$2/mo';

        planType.textContent='(Monthly)';
        totalPriceTag.textContent='Total (per month)';

        freeMonths.forEach((element)=>element.classList.add('displayOff'));

    }
}

// plan selector function
let selectedPlanCard=null;

function planSelector(selectedPlan)
{
    const planCards=document.querySelectorAll('.pCard');

    planCards.forEach((card)=> card.classList.remove('selectPlanDiv'));
    selectedPlan.classList.add("selectPlanDiv");
    selectedPlanCard=selectedPlan;
}

function planSelected()
{
    const errorMessage=document.getElementById('planError');

    if(selectedPlanCard===null)
    {
        errorMessage.classList.remove('displayOff');
    }
    else{
        errorMessage.classList.add('displayOff');
        nextPageFunction();
    }
}


function addOnSelector(selectedAddOn,check)
{
    selectedAddOn.classList.toggle('selectCheckbox');
    const checkbox = selectedAddOn.children[0];

    // console.log(check);
    if(check)
    {
        checkbox.checked = true;
    }
    else{
        checkbox.checked = !checkbox.checked;
    }
}


//summary page content

function summaryPageContent()
{
    const planName=document.getElementById('plan-name');
    const planPrice=document.getElementById('plan-pricing');

    let totalPrice=0;
    
    function hideAddOns() {
        for (let i = 1; i <= 3; i++) {
          document.getElementById(`addOn-${i}`).classList.add('displayOff');
          document.getElementById(`addOn-${i}-pricing`).classList.add('displayOff');
        }
    }
    hideAddOns();

    const alreadySelectedPlanName=selectedPlanCard.children[1].children[0].textContent;
    const alreadySelectedPlanPrice=selectedPlanCard.children[1].children[1].textContent;

    totalPrice=+alreadySelectedPlanPrice.replace("$",'').replace("yr","").replace("/",'');

    const alreadySelectedPlanType=document.getElementById('toggle').checked;


    const alreadySelectedAddOn=document.getElementsByClassName('selectCheckbox');
    // console.log(alreadySelectedAddOn);


    planName.textContent=alreadySelectedPlanName;
    planPrice.textContent=alreadySelectedPlanPrice;

    // console.log(alreadySelectedAddOn.length);
    
    for(let index=0;index<alreadySelectedAddOn.length;index++)
    {
        let addOnText=document.getElementById(`addOn-${index+1}`);
        let addOnPrice=document.getElementById(`addOn-${index+1}-pricing`);

        addOnText.classList.remove("displayOff");
        addOnPrice.classList.remove("displayOff");

        let alreadySelectedAddOnName=alreadySelectedAddOn[index].children[1].children[0].textContent;
        let alreadySelectedAddOnPrice=alreadySelectedAddOn[index].children[2].textContent;

        totalPrice+=+alreadySelectedAddOnPrice.replace("$",'').replace("yr","").replace("/",'');

        addOnText.textContent=alreadySelectedAddOnName;
        addOnPrice.textContent=alreadySelectedAddOnPrice;
    }

    if(alreadySelectedPlanType)
    {
        document.getElementById('totalPriceCost').textContent=`+$${totalPrice}/yr`;
    }
    else{
        document.getElementById('totalPriceCost').textContent=`+$${totalPrice}/mo`
    }
    

    nextPageFunction();
}


function backToPlanPage()
{
    const planPage=document.getElementById('plan');
    const summaryPage=document.getElementById('totalPricingSummary');

    const two=document.getElementById('2');
    const four=document.getElementById('4');

    planPage.classList.remove("displayOff");
    summaryPage.classList.add('displayOff');

    four.classList.remove('fillColor');
    two.classList.add('fillColor');
}